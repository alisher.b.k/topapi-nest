import { Body, Controller, Delete, Get, HttpException, HttpStatus, Logger, Param, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateReviewDto } from './dto/create-review.dto';
import { ReviewService } from './review.service';
import { REVIEW_NOT_FOUND, REVIEW_NOT_FOUND_BY_PRODUCTID } from './review.constants';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { IdValidationPipe } from 'src/pipes/id-validation.pipe';
import { TelegramService } from 'src/telegram/telegram.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@ApiTags('Top API')
@Controller('review')
export class ReviewController {

	constructor(
		private readonly reviewService: ReviewService,
		private readonly telegramService: TelegramService
	) { }

	@ApiBody({ type: CreateReviewDto })
	@UsePipes(new ValidationPipe())
	@Post('create')
	async create(@Body() dto: CreateReviewDto) {
		return this.reviewService.create(dto);
	}


	@ApiBody({ type: CreateReviewDto })
	@UsePipes(new ValidationPipe())
	@Post('notify')
	async notify(@Body() dto: CreateReviewDto) {
		const message = `Имя: ${dto.name}\n`
			+ `Заголовок: ${dto.title}\n`
			+ `Описание: ${dto.description}\n`
			+ `Рейтинг: ${dto.rating}\n`
			+ `Id продукта: ${dto.productId}`;
		return this.telegramService.senMessage(message)
	}

	@UseGuards(JwtAuthGuard)
	@Delete(':id')
	async delete(@Param('id', IdValidationPipe) id: string) {
		const deleted = await this.reviewService.delete(id);
		if (!deleted) {
			throw new HttpException(REVIEW_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		return deleted;
	}


	@Get('byProduct/:productId')
	async getByProduct(@Param('productId', IdValidationPipe) productId: string) {
		const result = await this.reviewService.findByProductId(productId);
		if (!result) {
			throw new HttpException(REVIEW_NOT_FOUND_BY_PRODUCTID, HttpStatus.NOT_FOUND);
		}
		return result;
	}
}
