import { IsEnum } from 'class-validator';
import { TopLevelCategory } from '../top-page.model';
import { ApiProperty } from '@nestjs/swagger';

export class FindTopPageDto {
	@ApiProperty({ enum: TopLevelCategory })
	@IsEnum(TopLevelCategory)
	firstCategory: TopLevelCategory;
}
