import { Type } from "class-transformer";
import { IsArray, IsEnum, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";
import { TopLevelCategory } from "../top-page.model";
import { ApiProperty } from "@nestjs/swagger";

export class TopPageAdvantageDto {

    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsString()
    description: string;
}

export class HhDataDto {
    @ApiProperty()
    @IsNumber()
    count: number;

    @ApiProperty()
    @IsNumber()
    juniorSalary: number;

    @ApiProperty()
    @IsNumber()
    middleSalary: number;

    @ApiProperty()
    @IsNumber()
    seniorSalary: number;
}

export class CreateTopPageDto {
    @ApiProperty({ enum: TopLevelCategory })
    @IsEnum(TopLevelCategory)
    firstCategory: TopLevelCategory;

    @ApiProperty()
    @IsString()
    secondCategory: string;

    @ApiProperty()
    @IsString()
    alias: string;

    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsString()
    category: string;

    @ApiProperty({ required: false })
    @IsOptional()
    @ValidateNested()
    @Type(() => HhDataDto)
    hh?: HhDataDto

    @ApiProperty({ isArray: true, type: TopPageAdvantageDto })
    @ValidateNested()
    @IsArray()
    @Type(() => TopPageAdvantageDto)
    advantages: TopPageAdvantageDto[];

    @ApiProperty()
    @IsString()
    seoText: string;

    @ApiProperty()
    @IsString()
    tagsTitle: string;

    @ApiProperty({ isArray: true, type: String })
    @IsArray()
    @IsString({ each: true })
    tags: string[];
}