import { ApiProperty } from "@nestjs/swagger";

export class FileElementResponse {
    @ApiProperty()
    url: string;

    @ApiProperty()
    name: string;
}