/* eslint-disable prettier/prettier */
import { BadRequestException, Body, Controller, HttpCode, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { ALREADY_REGISTERED_ERROR } from './auth.constants';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Type } from 'class-transformer';

@ApiTags('Top API')
@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) { }

    @ApiBody({ type: AuthDto })
    @ApiResponse({
        status: 201,
        description: 'Create User',
        type: AuthDto
    })
    @UsePipes(new ValidationPipe())
    @Post('register')
    async register(@Body() dto: AuthDto) {
        const oldUser = await this.authService.findUser(dto.login);
        if (oldUser) {
            throw new BadRequestException(ALREADY_REGISTERED_ERROR);
        }
        return await this.authService.createUser(dto);
    }

    @ApiBody({ type: AuthDto })
    @ApiResponse({
        status: 200,
        description: 'Login',
        type: Object
    })
    @ApiResponse({
        status: 401,
        description: 'Unauthorized',
        type: Object
    })
    @UsePipes(new ValidationPipe())
    @HttpCode(200)
    @Post('login')
    async login(@Body() { login, password }: AuthDto): Promise<{ access_token: string; }> {
        const { email } = await this.authService.validateUser(login, password);
        return this.authService.login(email);
    }
}
