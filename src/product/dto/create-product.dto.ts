import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";

export class ProductCharacteristicDto {

    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    value: string;
}

export class CreateProductDto {

    @ApiProperty()
    @IsString()
    image: string;

    @ApiProperty()
    @IsString()
    title: string;

    @ApiProperty()
    @IsNumber()
    price: number;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsNumber()
    oldPrice?: number;

    @ApiProperty()
    @IsNumber()
    credit: number;

    @ApiProperty()
    @IsString()
    description: string;

    @ApiProperty()
    @IsString()
    advantages: string;

    @ApiProperty()
    @IsString()
    disAdvantages: string;

    @ApiProperty({ isArray: true, type: String })
    @IsArray()
    @IsString({ each: true })
    categories: string[];

    @ApiProperty({ isArray: true, type: String })
    @IsArray()
    @IsString({ each: true })
    tags: string[];

    @ApiProperty({ isArray: true, type: ProductCharacteristicDto })
    @IsArray()
    @ValidateNested()
    @Type(() => ProductCharacteristicDto)
    characteristics: ProductCharacteristicDto[];
}