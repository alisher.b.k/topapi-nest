import { AuthDto } from "src/auth/dto/auth.dto";
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import * as request from 'supertest';
import { disconnect } from "mongoose";
import { USER_NOT_FOUND_ERROR, WRONG_PASSWORD_ERROR } from "../src/auth/auth.constants";



const authUser: AuthDto = {
    login: 'a@a.kz',
    password: '1'
}


describe('AuthController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });


    it('/auth/login - success', async () => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send(authUser)
            .expect(200)
            .then(({ body }: request.Response) => {
                expect(body.access_token).toBeDefined();
            });
    });

    it('/auth/login - password fail', async () => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send({ ...authUser, password: '12' })
            .expect(401)
            .then(({ body }: request.Response) => {
                expect(body.message).toBe(WRONG_PASSWORD_ERROR);
            });
    });

    it('/auth/login - email fail', async () => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send({ ...authUser, login: 'aasdsd@c.kz' })
            .expect(401)
            .then(({ body }: request.Response) => {
                expect(body.message).toBe(USER_NOT_FOUND_ERROR);
            });
    });

    afterAll(() => {
        disconnect();
    });
});